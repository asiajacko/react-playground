import React from "react";

export const Change = props => {
  return (
    <div>
      <h2>Change Component</h2>
      <ul>
        <a href="#">{props.homeLink}</a>

        <p>asia Tried:</p>

        <a href="#">{props.asiaLink}</a>
        <li />
      </ul>
    </div>
  );
};

export default Change;
