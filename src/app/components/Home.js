import React from "react";
// import PropTypes from "prop-types";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.onMakeOlder = this.onMakeOlder.bind(this);
    this.state = {
      age: props.initialAge,
      name: "John",
      status: 0,
      homeLink: "Change Link",
      asiaLink: "no Contact!!"
    };

    setTimeout(() => {
      this.setState({
        status: 1
      });
    }, 3000);
  }

  onMakeOlder() {
    this.setState({
      age: this.state.age + 3,
      name: (this.state.name, "sthsth")
    });
  }
  onChangeLink() {
    this.props.changeLink(this.state.homeLink);
  }

  onChangeHandler(event) {
    this.setState({
      homeLink: event.target.value
    });
  }

  changeContactName() {
    this.props.changeContact(this.state.asiaLink);
  }

  onChangeContact(event) {
    this.setState({
      asiaLink: event.target.value
    });
  }
  render() {
    return (
      <div>
        <h2>your name is {this.props.name}</h2>
        <p>your age is {this.props.user.age}</p>

        <h2>Timeout:{this.state.status}</h2>
        <h2>your name is {this.state.name}</h2>
        <p>Your age is {this.state.age}</p>
        <div />
        <button onClick={this.props.greet}>Make Hello</button>
        <button onClick={this.onMakeOlder}>Make me Older</button>
        <button onClick={this.onChangeLink.bind(this)}>Change Link</button>

        <input
          type="text"
          value={this.state.homeLink}
          onChange={event => this.onChangeHandler(event)}
        />

        <p>Asia try:</p>
        <button onClick={this.changeContactName.bind(this)}>
          Change Contact
        </button>

        <input
          type="text"
          value={this.state.asiaLink}
          onChange={event => this.onChangeContact(event)}
        />
      </div>
    );
  }
}

// Home.propTypes = {
//   name: React.PropTypes.string,
//   age: React.PropTypes.number,
//   user: React.PropTypes.object,
//   children: React.PropTypes.element.isRequired
// };

export default Home;
