import React from "react";

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const text = this.props.paragraph;
    const lengthMax = 20;

    return (
      <div>
        <h2>Not Found!111!!</h2>
        <p>{this.props.paragraph}</p>

        <div>
          {text.length > lengthMax ? (
            <div>{`${text.substring(0, lengthMax)}...`}</div>
          ) : (
            <p>{text}</p>
          )}
        </div>
      </div>
    );
  }
}

export default Header;
