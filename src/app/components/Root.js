import React from "react";

import HeaderAsia from "./HeaderAsia";

export class Root extends React.Component {
  render() {
    return (
      <div>
        <HeaderAsia />
        {this.props.children}
      </div>
    );
  }
}

export default Root;
