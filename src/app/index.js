console.log("sss");

import React from "react";
import { render } from "react-dom";

import Header from "./components/Header";
import Home from "./components/Home";
import Change from "./components/Change";

import Root from "./components/Root";
import House from "./components/House";
import User from "./components/User";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      homeLink: "Home",
      asiaLink: "Contact"
    };
  }
  onGreet() {
    alert("smallAlert");
  }
  onChangeLinkName(newName) {
    this.setState({
      homeLink: newName
    });
  }

  onChangeContactkName(newContact) {
    this.setState({
      asiaLink: newContact
    });
  }

  render() {
    const user = {
      name: "Anna",
      age: "33",
      place: "Thaiti",
      hobbies: ["Sports", "Piano", "Guitar"]
    };
    return (
      <div>
        <Change homeLink={this.state.homeLink} asiaLink={this.state.asiaLink} />
        <Header paragraph={"Lorem Ipsum fldha djhfi aoutwi iwudhjxbc"} />
        <Home
          name={"Maxw"}
          user={user}
          initialAge={21}
          greet={this.onGreet}
          changeLink={this.onChangeLinkName.bind(this)}
          initialLinkName={this.state.homeLink}
          changeContact={this.onChangeContactkName.bind(this)}
          // currentContactName={this.state.asiaLink}
        />
      </div>
    );
  }
}

render(<App />, document.querySelector("#app"));
